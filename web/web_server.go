package web

import (
	"net/http"
	"strconv"

	"gitee.com/zhuxmhz/pigo-base/logger"
)

func Run(userConfig *Config) {
	loadConfig(userConfig)

	http.HandleFunc("/", handler)

	httpListener()
}

func loadConfig(userConfig *Config) {
	if userConfig == nil {
		userConfig = &Config{
			Port: 8080,
		}
	}
	if userConfig.Port < 1 || userConfig.Port > 65535 {
		accessLog.Fatal(nil, "Listen Port Error : "+strconv.Itoa(cfg.Port))
	}
	cfg = userConfig
	if userConfig.AccessLogConfig == nil {
		accessLog, _ = logger.NewLogger(logger.Config{
			Name: "access",
		})
	} else {
		accessLog, _ = logger.NewLogger(*userConfig.AccessLogConfig)
	}
}

func httpListener() {
	if len(cfg.CertFile) != 0 && len(cfg.KeyFile) != 0 {
		accessLog.Info(nil, "Https Server Listening : %d", cfg.Port)
		err := http.ListenAndServeTLS(":"+strconv.Itoa(cfg.Port), cfg.CertFile, cfg.KeyFile, nil)
		if err != nil {
			accessLog.Fatal(nil, "start https server error : [%v]", err)
		}
	} else {
		accessLog.Info(nil, "Http Server Listening : %d", cfg.Port)
		err := http.ListenAndServe(":"+strconv.Itoa(cfg.Port), nil)
		if err != nil {
			accessLog.Fatal(nil, "start http server error : [%v]", err)
		}
	}
}
