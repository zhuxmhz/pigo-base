package web

import (
	"gitee.com/zhuxmhz/pigo-base/logger"
)

type Config struct {
	Port            int
	CertFile        string
	KeyFile         string
	AccessLogConfig *logger.Config
}

var cfg *Config
var accessLog *logger.LogFile
