package web

import (
	"context"
	"net/http"
	"regexp"
)

type httpHandler func(ctx *context.Context) (statusCode int, data interface{})

var routerMap = map[string]map[string]httpHandler{}
var regRouterMap = map[string]string{}

// Router 注册一个方法到某个url
func Router(url string, handler httpHandler, methods ...string) {
	reg := regexp.MustCompile(`\{[0-9A-Za-z]*\}`)
	regUrl := reg.ReplaceAllString(url, "[0-9A-Za-z]*")
	if len(methods) == 0 {
		methods = append(methods, http.MethodGet, http.MethodPost, http.MethodPatch, http.MethodPut, http.MethodDelete)
	}
	for _, method := range methods {
		if routerMap[regUrl] == nil {
			routerMap[regUrl] = map[string]httpHandler{}
		}
		if routerMap[regUrl][method] != nil {
			accessLog.Fatal(nil, "Duplicate Url : [ %s %s ] and [ %s %s ]", method, regRouterMap[regUrl], method, url)
		}
		regRouterMap[regUrl] = url
		routerMap[regUrl][method] = handler
	}
}

// RestRouter 以Rest风格注册一个WebController
func RestRouter(baseUrl string, ctl IController, notApi ...string) {
	config := map[string]bool{}
	for _, not := range notApi {
		config[not] = true
	}
	if !config[NotInsert] {
		Router(baseUrl, ctl.Insert, http.MethodPost)
	}
	if !config[NotDelete] {
		Router(baseUrl+"/{id}", ctl.DeleteById, http.MethodDelete)
	}
	if !config[NotUpdate] {
		Router(baseUrl+"/{id}", ctl.UpdateById, http.MethodPatch, http.MethodPut)
	}
	if !config[NotDetail] {
		Router(baseUrl+"/{id}", ctl.SelectById, http.MethodGet)
	}
	if !config[NotList] {
		Router(baseUrl, ctl.SelectList, http.MethodGet)
	}
}
