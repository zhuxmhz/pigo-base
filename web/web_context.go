package web

import (
	"context"
	"encoding/json"
	"io"
)

func parsePathParam(ctx *context.Context, matchUrl string) {
	req := GetRequestFromContext(ctx)
	pathParams := map[string]interface{}{}
	url := req.URL.Path
	base := []byte(matchUrl)
	match := []byte(url)
	for i, j := 0, 0; i < len(base) && j < len(match); {
		if base[i] == match[j] {
			i++
			j++
		} else {
			key := ""
			if base[i] == '{' {
				i++
				for i < len(base) && base[i] != '}' {
					key += string(base[i])
					i++
				}
				i++
			}
			val := ""
			for j < len(match) && match[j] != '/' {
				val += string(match[j])
				j++
			}
			pathParams[key] = val
		}
	}
	SetMapParamsToContext(ctx, CtxValuePathParams, pathParams)
}

func parseQueryParam(ctx *context.Context) {
	req := GetRequestFromContext(ctx)
	queryParam := map[string]interface{}{}
	for k, v := range req.URL.Query() {
		if l := len(v); l == 1 {
			queryParam[k] = v[0]
		} else {
			queryParam[k] = v
		}
	}
	SetMapParamsToContext(ctx, CtxValueQueryParams, queryParam)
}

func parseBodyParam(ctx *context.Context) {
	req := GetRequestFromContext(ctx)
	bodyParam := map[string]interface{}{}
	body, err := io.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	SetParamsToContext(ctx, CtxValueRequestBody, string(body))
	if len(body) == 0 {
		return
	}
	err = json.Unmarshal(body, &bodyParam)
	if err != nil {
		panic(err)
	}
	SetMapParamsToContext(ctx, CtxValueBodyParams, bodyParam)
}
