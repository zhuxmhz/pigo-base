package web

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
)

func handler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	defer func() {
		err := recover()
		if err != nil {
			responseWriter(&ctx, http.StatusInternalServerError, fmt.Sprintf("%s", err))
		}
		return
	}()

	status, respBody := runBeforeServer(&ctx, r, w)
	if respBody != nil {
		responseWriter(&ctx, status, respBody)
		return
	}

	url := r.URL.Path
	method := r.Method

	for routerUrl, methodMap := range routerMap {
		if !checkUrl(url, routerUrl) {
			continue
		}
		parsePathParam(&ctx, regRouterMap[routerUrl])
		if checkMethod(methodMap, method) {
			code, resp := methodMap[method](&ctx)
			responseWriter(&ctx, code, resp)
			return
		} else {
			responseWriter(&ctx,
				http.StatusMethodNotAllowed,
				"405 Method Not Allowed : "+GetRequestFromContext(&ctx).URL.Path)
			return
		}
	}
	responseWriter(&ctx,
		http.StatusNotFound,
		"404 Not Found : "+GetRequestFromContext(&ctx).URL.Path)
	return
}

func responseWriter(ctx *context.Context, statusCode int, data interface{}) {
	SetParamsToContext(ctx, CtxValueResponseCode, statusCode)
	SetParamsToContext(ctx, CtxValueResponseBody, data)
	respWriter := GetResponseWriterFromContext(ctx)
	req := GetRequestFromContext(ctx)
	status, respBody := runAfterServer(ctx, req, respWriter)
	if respBody != nil {
		statusCode = status
		data = respBody
	}
	respWriter.WriteHeader(statusCode)
	if data == nil {
		return
	}
	var respBytes []byte
	if dataStr, isStr := data.(string); isStr {
		respBytes = []byte(dataStr)
	} else if dataBytes, isBytes := data.([]byte); isBytes {
		respBytes = dataBytes
	} else {
		marshal, err := json.Marshal(data)
		if err != nil {
			panic(err)
		}
		respBytes = marshal
	}
	_, err := respWriter.Write(respBytes)
	if err != nil {
		panic(err)
	}
}

func checkUrl(url string, routerUrl string) bool {
	reg := regexp.MustCompile("^" + routerUrl + "$")
	return reg.MatchString(url)
}

func checkMethod(methodMap map[string]httpHandler, method string) bool {
	if methodMap[method] != nil {
		return true
	}
	return false
}
