package web

import (
	"context"
)

type IController interface {
	Insert(ctx *context.Context) (int, interface{})
	DeleteById(ctx *context.Context) (int, interface{})
	UpdateById(ctx *context.Context) (int, interface{})
	SelectById(ctx *context.Context) (int, interface{})
	SelectList(ctx *context.Context) (int, interface{})
}

const (
	NotInsert = "NotInsert"
	NotDelete = "NotDelete"
	NotUpdate = "NotUpdate"
	NotDetail = "NotDetail"
	NotList   = "NotList"
)
