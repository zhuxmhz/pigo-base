package web

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func init() {
	BeforeServer("init-context", contextInitChain)
	AfterServer("access-log", accessLogChain)
}

type httpChain func(ctx *context.Context, req *http.Request, resp http.ResponseWriter) (int, interface{})

var beforeChainList []httpChain
var afterChainList []httpChain
var chainNameMap = map[string]httpChain{}

// BeforeServer 注册一个方法到处理之前，先注册、先执行
func BeforeServer(name string, chain httpChain) {
	if _, has := chainNameMap[name]; has {
		panic(fmt.Sprintf("Duplicate Before Server Chain : %v", name))
	} else {
		chainNameMap[name] = chain
	}
	beforeChainList = append(beforeChainList, chain)
}

func contextInitChain(ctx *context.Context, req *http.Request, resp http.ResponseWriter) (int, interface{}) {
	SetRequestToContext(ctx, req)
	SetResponseWriterToContext(ctx, resp)
	SetParamsToContext(ctx, CtxValueStartTime, time.Now())
	parseQueryParam(ctx)
	parseBodyParam(ctx)
	return 0, nil
}

// AfterServer 注册一个方法到处理之后，先注册、后执行
func AfterServer(name string, chain httpChain) {
	if _, has := chainNameMap[name]; has {
		panic(fmt.Sprintf("Duplicate After Server Chain : %v", name))
	} else {
		chainNameMap[name] = chain
	}
	newChainList := []httpChain{chain}
	afterChainList = append(newChainList, afterChainList...)
}

func accessLogChain(ctx *context.Context, req *http.Request, resp http.ResponseWriter) (int, interface{}) {
	startTime, _ := GetParamsFromContext(ctx, CtxValueStartTime).(time.Time)
	accessLog.Duration(startTime).
		WithParam(CtxValueRequestBody, GetParamsFromContext(ctx, CtxValueRequestBody)).
		WithParam(CtxValueRequestUri, req.URL.Path).
		WithParam(CtxValueRequestQuery, req.URL.RawQuery).
		WithParam(CtxValueResponseCode, GetParamsFromContext(ctx, CtxValueResponseCode)).
		WithParam(CtxValueResponseBody, GetParamsFromContext(ctx, CtxValueResponseBody)).
		Info(ctx, "handler [%v %v]", req.Method, req.URL.Path)
	return 0, nil
}

func runBeforeServer(ctx *context.Context, req *http.Request, resp http.ResponseWriter) (int, interface{}) {
	for _, chain := range beforeChainList {
		status, respBody := chain(ctx, req, resp)
		if respBody != nil {
			return status, respBody
		}
	}
	return 0, nil
}

func runAfterServer(ctx *context.Context, req *http.Request, resp http.ResponseWriter) (int, interface{}) {
	for _, chain := range afterChainList {
		status, respBody := chain(ctx, req, resp)
		if respBody != nil {
			return status, respBody
		}
	}
	return 0, nil
}
