package web

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
)

const (
	CtxValueRequest      = "request"
	CtxValueResponse     = "response"
	CtxValuePathParams   = "path_params"
	CtxValueQueryParams  = "query_params"
	CtxValueBodyParams   = "body_params"
	CtxValueRequestUri   = "request_uri"
	CtxValueRequestBody  = "request_body"
	CtxValueRequestQuery = "request_query"
	CtxValueResponseCode = "response_code"
	CtxValueResponseBody = "response_body"
	CtxValueStartTime    = "start_time"
)

func ReadBodyEntity(ctx *context.Context, data interface{}) error {
	body := GetParamsFromContext(ctx, CtxValueRequestBody)
	bodyStr, ok := body.(string)
	if !ok {
		return errors.New("body parse err")
	}
	return json.Unmarshal([]byte(bodyStr), data)
}

func SetRequestToContext(ctx *context.Context, req *http.Request) {
	*ctx = context.WithValue(*ctx, CtxValueRequest, req)
}

func GetRequestFromContext(ctx *context.Context) *http.Request {
	if req, ok := (*ctx).Value(CtxValueRequest).(*http.Request); ok {
		return req
	}
	return nil
}

func SetResponseWriterToContext(ctx *context.Context, resp http.ResponseWriter) {
	*ctx = context.WithValue(*ctx, CtxValueResponse, resp)
}

func GetResponseWriterFromContext(ctx *context.Context) http.ResponseWriter {
	if resp, ok := (*ctx).Value(CtxValueResponse).(http.ResponseWriter); ok {
		return resp
	}
	return nil
}

func SetMapParamsToContext(ctx *context.Context, paramType string, params map[string]any) {
	*ctx = context.WithValue(*ctx, paramType, params)
}

func GetMapParamsFromContext(ctx *context.Context, paramType string) map[string]any {
	if params, ok := (*ctx).Value(paramType).(map[string]any); ok {
		return params
	}
	return nil
}

func SetParamsToContext(ctx *context.Context, paramType string, params any) {
	*ctx = context.WithValue(*ctx, paramType, params)
}

func GetParamsFromContext(ctx *context.Context, paramType string) any {
	if params, ok := (*ctx).Value(paramType).(any); ok {
		return params
	}
	return nil
}
