package main

import (
	"context"
	"net/http"
	"time"

	"gitee.com/zhuxmhz/pigo-base/web"
)

func init() {
	web.Router("/ping", Ping, http.MethodGet)
	web.Router("/post", Insert, http.MethodPost)
	web.Router("/panic", MockPanic, http.MethodGet)
}

func Insert(ctx *context.Context) (int, interface{}) {
	time.Sleep(10 * time.Millisecond)
	return http.StatusOK, web.GetMapParamsFromContext(ctx, web.CtxValueBodyParams)
}

func Ping(ctx *context.Context) (int, interface{}) {
	return http.StatusOK, map[string]interface{}{
		"status": "OK",
	}
}

func MockPanic(ctx *context.Context) (int, interface{}) {
	panic("something error")
}
