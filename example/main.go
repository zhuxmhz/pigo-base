package main

import (
	"gitee.com/zhuxmhz/pigo-base/logger"
	"gitee.com/zhuxmhz/pigo-base/web"
)

func main() {
	_ = logger.LoadConf(logger.GlobalConfig{
		Level: logger.LogLevelDebug,
	})

	runLog, _ := logger.NewLogger(logger.Config{
		Name: "run",
	})

	runLog.Info(nil, "logger init success")

	web.Run(&web.Config{
		Port: 8888,
	})
}
