package main

import (
	"context"
	"net/http"

	"gitee.com/zhuxmhz/pigo-base/web"
)

func init() {
	web.RestRouter("/user", User{}, web.NotList)
}

type User struct {
	Name string `json:"name"`
}

func (User) Insert(ctx *context.Context) (int, interface{}) {
	var user User
	err := web.ReadBodyEntity(ctx, &user)
	if err != nil {
		return http.StatusBadRequest, nil
	}
	return http.StatusOK, user
}

func (User) DeleteById(ctx *context.Context) (int, interface{}) {
	// TODO implement me
	panic("implement me")
}

func (User) UpdateById(ctx *context.Context) (int, interface{}) {
	// TODO implement me
	panic("implement me")
}

func (User) SelectById(ctx *context.Context) (int, interface{}) {
	// TODO implement me
	panic("implement me")
}

func (User) SelectList(ctx *context.Context) (int, interface{}) {
	// TODO implement me
	panic("implement me")
}
