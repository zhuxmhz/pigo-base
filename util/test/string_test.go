package test

import (
	"strings"
	"testing"

	"gitee.com/zhuxmhz/pigo-base/util"
)

func TestUUID(t *testing.T) {
	uid := util.GetUUID()
	uid2 := util.GetUUID()
	if uid == uid2 {
		t.Errorf("get uuid err: [%v], [%v]", uid, uid2)
		return
	}
	t.Logf("get uuid success: [%v], [%v]", uid, uid2)

	uid3 := util.GetShortUUID()
	if strings.Index(uid3, "-") != -1 {
		t.Errorf("get short uuid err: [%v]", uid3)
		return
	}
	t.Logf("get short uuid success: [%v]", uid3)
}
