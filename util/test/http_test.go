package test

import (
	"net/http"
	"testing"

	"gitee.com/zhuxmhz/pigo-base/util"
)

func TestSendRequest(t *testing.T) {
	req, err := util.NewRequest(http.MethodGet, "https://www.baidu.com", nil, nil, nil)
	if err != nil {
		t.Errorf("new request err: [%v]", err)
		return
	}
	t.Logf("new request success: [%v]", req.URL)

	status, body, err := util.SendRequest(req)
	if err != nil {
		t.Errorf("send request err: [%v]", err)
		return
	}
	t.Logf("send request success: [%v] body length - [%v]", status, len(string(body)))
}

func TestSendRequestWithParam(t *testing.T) {
	req, err := util.NewRequest(http.MethodGet, "https://www.baidu.com",
		map[string]string{"Content-Type": "application/json"},
		map[string]interface{}{"name": "test"},
		nil)
	if err != nil {
		t.Errorf("new request err: [%v]", err)
		return
	}
	t.Logf("new request success: [%v]", req.URL)

	status, body, err := util.SendRequest(req)
	if err != nil {
		t.Errorf("send request err: [%v]", err)
		return
	}
	t.Logf("send request success: [%v] body length - [%v]", status, len(string(body)))
}
