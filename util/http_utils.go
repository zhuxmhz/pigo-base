package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

var client *http.Client

func init() {
	client = &http.Client{}
}

func NewRequest(method string, uri string, header map[string]string, params map[string]interface{}, body interface{}) (req *http.Request, err error) {
	var bodyBytes []byte
	if body != nil {
		bodyBytes, err = json.Marshal(body)
		if err != nil {
			return nil, err
		}
	}
	var url = uri
	for k, v := range params {
		if strings.Index(url, string(FlagQuestion)) != -1 {
			url += fmt.Sprintf("&%s=%v", k, v)
		} else {
			url += fmt.Sprintf("?%s=%v", k, v)
		}
	}
	req, err = http.NewRequest(method, url, bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}
	for k, v := range header {
		req.Header.Set(k, v)
	}
	return req, nil
}

func SendRequest(req *http.Request) (int, []byte, error) {
	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp.StatusCode, respBody, err
	}
	return resp.StatusCode, respBody, err
}
