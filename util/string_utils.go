package util

import (
	"strings"

	"github.com/gofrs/uuid"
)

func GetUUID() string {
	uid, err := uuid.NewV7()
	if err != nil {
		return ""
	}
	return uid.String()
}

func GetShortUUID() string {
	uid, err := uuid.NewV7()
	if err != nil {
		return ""
	}
	return strings.ReplaceAll(uid.String(), string(FlagDash), "")
}
