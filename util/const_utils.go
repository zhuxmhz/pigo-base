package util

const (
	FlagDot         = '.'
	FlagComma       = ','
	FlagColon       = ':'
	FlagSemicolon   = ';'
	FlagQuestion    = '?'
	FlagAnd         = '&'
	FlagExclamation = '!'
	FlagPlus        = '+'
	FlagDash        = '-'
	FlagStar        = '*'
	FlagSlash       = '/'
	FlagEqual       = '='
	FlagLess        = '<'
	FlagMore        = '>'
	FlagVertical    = '|'
	FlagPound       = '#'
	FlagUnderline   = '_'
	FlagEnter       = '\n'
)
