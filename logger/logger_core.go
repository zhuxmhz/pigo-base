package logger

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"runtime"
	"strings"
	"time"

	"gitee.com/zhuxmhz/pigo-base/util"
)

var globalConf = GlobalConfig{
	Params:     map[string]any{},
	TimeFormat: RFC3339Mill,
	Level:      LogLevelInfo,
}

type LogFile struct {
	conf         Config
	file         io.Writer
	out          io.Writer
	timeFormat   string
	tempParamMap map[string]any
}

const (
	logParams        = "logger_params"
	logParamMessage  = "log_msg"
	logParamTime     = "log_time"
	logParamLevel    = "log_level"
	logParamLocation = "log_location"
	logParamName     = "log_name"
	logParamDuration = "duration_ms"

	defaultLogDirPath = "./logs"
	RFC3339Mill       = "2006-01-02T15:04:05.999Z07:00"
)

var logLevelMap = map[string]int{
	LogLevelDebug:   0,
	LogLevelInfo:    1,
	LogLevelWarn:    2,
	LogLevelError:   3,
	LogLevelDisable: 4,
}

func checkAndFormatLogConf(conf *GlobalConfig) error {
	conf.Level = strings.ToLower(conf.Level)
	if len(conf.Level) == 0 {
		conf.Level = LogLevelDebug
	} else if _, ok := logLevelMap[conf.Level]; !ok {
		return ErrLogLevelInvalid
	}
	if globalConf.Params == nil {
		globalConf.Params = map[string]any{}
	}
	if len(conf.TimeFormat) == 0 {
		conf.TimeFormat = globalConf.TimeFormat
	}
	return nil
}

func checkAndFormatLogFileConf(conf *Config) error {
	if len(conf.Level) == 0 {
		conf.Level = globalConf.Level
	}
	conf.Level = strings.ToLower(conf.Level)
	if len(conf.Name) == 0 {
		return ErrLogNameEmpty
	}
	if len(conf.DirPath) == 0 {
		conf.DirPath = defaultLogDirPath
	}
	if _, ok := logLevelMap[conf.Level]; !ok && len(conf.Level) != 0 {
		return ErrLogLevelInvalid
	}
	if conf.Params == nil {
		conf.Params = map[string]any{}
	}
	if len(conf.TimeFormat) == 0 {
		conf.TimeFormat = globalConf.TimeFormat
	}
	return nil
}

func (l LogFile) parseCtx(ctx *context.Context, logLevel string, format string, params ...any) map[string]any {
	if ctx == nil {
		newCtx := context.Background()
		ctx = &newCtx
	}
	paramMap := map[string]any{}
	for k, v := range globalConf.Params {
		paramMap[k] = v
	}
	for k, v := range l.conf.Params {
		paramMap[k] = v
	}
	for k, v := range l.tempParamMap {
		paramMap[k] = v
	}
	paramMap[logParamMessage] = fmt.Sprintf(format, params...)
	paramMap[logParamName] = l.conf.Name
	paramMap[logParamTime] = time.Now().Format(globalConf.TimeFormat)
	paramMap[logParamLevel] = logLevel
	pc, file, line, _ := runtime.Caller(2)
	funcName := runtime.FuncForPC(pc).Name()
	split := strings.Split(funcName, string(util.FlagDot))
	funcName = split[len(split)-1]
	split = strings.Split(file, string(util.FlagSlash))
	file = split[len(split)-1]
	paramMap[logParamLocation] = fmt.Sprintf("%s:%d %s", file, line, funcName)
	if m, ok := (*ctx).Value(logParams).(map[string]any); ok {
		for k, v := range m {
			paramMap[k] = v
		}
	}
	return paramMap
}

func (l LogFile) needLog(logLevel string) bool {
	var enableLevel = l.conf.Level
	if len(enableLevel) == 0 {
		enableLevel = globalConf.Level
	}
	return logLevelMap[logLevel] >= logLevelMap[enableLevel]
}

func (l LogFile) writeJsonLog(paramMap map[string]any) {
	bytes, err := json.Marshal(paramMap)
	if err != nil {
		return
	}
	bytes = append(bytes, util.FlagEnter)
	_, err = l.out.Write(bytes)
	if err != nil {
		return
	}
}
