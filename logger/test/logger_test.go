package test

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitee.com/zhuxmhz/pigo-base/logger"
	"gitee.com/zhuxmhz/pigo-base/util"
)

func TestLog(t *testing.T) {
	startTime := time.Now()
	rand.Seed(time.Now().UnixNano())
	err := logger.LoadConf(logger.GlobalConfig{
		Level: logger.LogLevelDebug,
		Params: map[string]any{
			"endpoint": "localhost",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	debugLog, err := logger.NewLogger(logger.Config{
		Name:  "access",
		Level: logger.LogLevelDebug,
		Params: map[string]any{
			"request_id": util.GetShortUUID(),
			"is_bool":    true,
		},
	})
	if err != nil {
		t.Error(err)
	}

	ctx := context.Background()
	logger.AddParam(&ctx, "test_param", "zhuxm")
	debugLog.Debug(&ctx, "test format %s", "666")

	warnLog, err := logger.NewLogger(logger.Config{
		Name: "monitor",
	})
	if err != nil {
		t.Error(err)
		fmt.Printf("%s", "666")
	}
	warnLog.Info(nil, "test %v\n", "warn test %2B\n")
	warnLog.Duration(startTime).Error(nil, "error [%s]", "something error")
	warnLog.WithParam("test", "666").Duration(startTime).Error(nil, "error [%s]", "something error")
	warnLog.Error(nil, "error [%s]", "something error")
}
