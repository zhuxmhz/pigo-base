package logger

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

// GlobalConfig 日志全局配置
type GlobalConfig struct {
	Level       string         // Level 日志记录级别
	Params      map[string]any // Params 全局日志默认参数
	MaxSize     int            // MaxSize 单个日志最大大小，达到大小压缩当前日志文件并创建新文件，单位MB
	RotateCycle int            // RotateCycle 日志压缩周期，达到周期后压缩当前日志文件并创建新文件，单位day，默认1
	DeleteCycle int            // DeleteCycle 日志删除周期，达到周期后删除过期日志，单位day，默认1
	TimeFormat  string         // TimeFormat 时间格式化格式
}

// Config 日志文件配置，优先级高于全局配置
type Config struct {
	Name          string         // Name 日志名称，用于分类和创建日志文件
	Level         string         // Level 日志记录级别
	Params        map[string]any // Params 该类日志默认参数
	DirPath       string         // DirPath 日志存放路径
	DisableStdout bool           // DisableStdout 是否输出到控制台
	TimeFormat    string         // TimeFormat 时间格式化格式
}

var (
	ErrLogNameEmpty    = errors.New("init logger error : Name can not be empty") // ErrLogNameEmpty 日志文件名为空错误
	ErrLogLevelInvalid = errors.New("init logger error : Level is invalid")      // ErrLogLevelInvalid 日志级别无效
)

const (
	LogLevelDisable = "disable" // LogLevelDisable 日志级别：禁用日志
	LogLevelError   = "error"   // LogLevelError 日志级别：只记录error日志
	LogLevelWarn    = "warn"    // LogLevelWarn 日志级别：记录warn、error日志
	LogLevelInfo    = "info"    // LogLevelInfo 日志级别：记录info、warn、error日志
	LogLevelDebug   = "debug"   // LogLevelInfo 日志级别：记录所有日志
)

// LoadConf 加载全局配置
func LoadConf(conf GlobalConfig) error {
	err := checkAndFormatLogConf(&conf)
	if err != nil {
		return err
	}
	globalConf.Level = conf.Level
	globalConf.Params = conf.Params
	globalConf.TimeFormat = conf.TimeFormat
	return nil
}

// NewLogger 创建新的日志文件
func NewLogger(conf Config) (*LogFile, error) {
	err := checkAndFormatLogFileConf(&conf)
	if err != nil {
		return nil, err
	}
	_, err = os.Stat(conf.DirPath)
	if os.IsNotExist(err) {
		err = os.MkdirAll(conf.DirPath, os.ModePerm)
		if err != nil {
			return nil, err
		}
	}

	fileName := fmt.Sprintf("%s/%s.log", conf.DirPath, conf.Name)
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModePerm)

	var out io.Writer
	if !conf.DisableStdout {
		out = io.MultiWriter(os.Stdout, file)
	} else {
		out = file
	}

	var logFile = &LogFile{
		conf: conf,
		file: file,
		out:  out,
	}
	return logFile, nil
}

// AddParam 为context添加日志字段
func AddParam(ctx *context.Context, key string, value any) {
	if m, ok := (*ctx).Value(logParams).(map[string]any); ok {
		m[key] = value
	} else {
		m = map[string]any{
			key: value,
		}
		*ctx = context.WithValue(*ctx, logParams, m)
	}
}

// Fatal 记录fatal日志
func (l LogFile) Fatal(ctx *context.Context, format string, params ...any) {
	paramMap := l.parseCtx(ctx, LogLevelError, format, params...)
	l.writeJsonLog(paramMap)
	log.Fatalln("exit")
}

// Error 记录error日志
func (l LogFile) Error(ctx *context.Context, format string, params ...any) {
	if !l.needLog(LogLevelError) {
		return
	}
	paramMap := l.parseCtx(ctx, LogLevelError, format, params...)
	l.writeJsonLog(paramMap)
}

// Warn 记录warn日志
func (l LogFile) Warn(ctx *context.Context, format string, params ...any) {
	if !l.needLog(LogLevelWarn) {
		return
	}
	paramMap := l.parseCtx(ctx, LogLevelWarn, format, params...)
	l.writeJsonLog(paramMap)
}

// Info 记录info日志
func (l LogFile) Info(ctx *context.Context, format string, params ...any) {
	if !l.needLog(LogLevelInfo) {
		return
	}
	paramMap := l.parseCtx(ctx, LogLevelInfo, format, params...)
	l.writeJsonLog(paramMap)
}

// Debug 记录debug日志
func (l LogFile) Debug(ctx *context.Context, format string, params ...any) {
	if !l.needLog(LogLevelDebug) {
		return
	}
	paramMap := l.parseCtx(ctx, LogLevelDebug, format, params...)
	l.writeJsonLog(paramMap)
}

// WithParam 记录日志前添加自定义参数
func (l LogFile) WithParam(key string, value any) LogFile {
	if l.tempParamMap == nil {
		l.tempParamMap = map[string]any{}
		newLogFile := LogFile{
			conf:         l.conf,
			file:         l.file,
			out:          l.out,
			tempParamMap: l.tempParamMap,
		}
		newLogFile.tempParamMap[key] = value
		return newLogFile
	} else {
		l.tempParamMap[key] = value
		return l
	}
}

// Duration 根据开始时间，计算duration并纳入自定义参数
func (l LogFile) Duration(startTime time.Time) LogFile {
	return l.WithParam(logParamDuration, time.Now().Sub(startTime).Milliseconds())
}
